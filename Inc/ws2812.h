#ifndef WS2812_WS2812_H_
#define WS2812_WS2812_H_

#include <stdint.h>

// #include "stm32f1xx_hal.h"

#if defined(STM32C031xx)
#include "stm32c0xx_hal.h"
#endif

namespace WS2812 {

struct Rgb {
	uint8_t r;
	uint8_t g;
	uint8_t b;
};

class Ws2812Impl
{
public:
	Ws2812Impl() = default;
	void setupPwmTimer(TIM_HandleTypeDef *htimer, uint32_t timer_channel, unsigned long clock_hz);
	void setupPwmData(uint16_t* pwm_data, unsigned int total_bits, unsigned int preroll_bits, unsigned int data_bits);

	void setLedColor(unsigned int led_index, uint8_t r,	uint8_t g, uint8_t b);

	void update();

private:
	uint16_t* m_pwm_data;
	unsigned int m_total_bits;
	unsigned int m_preroll_bits;
	unsigned int m_data_bits;

	TIM_HandleTypeDef *m_htimer;
	uint32_t m_timer_channel;

	uint16_t m_timer_autoreload_reg;
	uint16_t m_timer_pwm_0;
	uint16_t m_timer_pwm_1;

	void setPwmData(unsigned int index, uint8_t color);
};

/**
 * NUMBER_OF_LEDS is the (maximal) length of your LED strip.
 */
template <unsigned int NUMBER_OF_LEDS>
class Ws2812
{
public:
	Ws2812() = default;

	void setup(TIM_HandleTypeDef *htimer, uint32_t timer_channel, unsigned long clock_hz)
	{
		ws2812_impl.setupPwmTimer(htimer, timer_channel, clock_hz);
		ws2812_impl.setupPwmData(pwm_data, PWM_BITS, PREROLL_BITS, PWM_DATA_BITS);
	}

	void setLedColor(unsigned int led_index, uint8_t r,	uint8_t g, uint8_t b, bool update = true)
	{
		ws2812_impl.setLedColor(led_index, r, g, b);
		if (update) {
			ws2812_impl.update();
		}
	}

	void setLedColor(unsigned int led_index, Rgb * color, bool update = true)
	{
		setLedColor(led_index, color->r, color->g, color->b, update);
	}

	void update()
	{
		ws2812_impl.update();
	}

private:
	/** RGB LEDs transfer 3 channels each */
	static const unsigned int NUMBER_OF_COLOR_CHANNELS = 3;

	/** WS2812 require 8 bits per color */
	static const unsigned int NUMBER_OF_BITS_PER_CHANNEL = 8;

	/** Bits used to transfer LED color information */
	static const unsigned int PWM_DATA_BITS = NUMBER_OF_LEDS * NUMBER_OF_COLOR_CHANNELS * NUMBER_OF_BITS_PER_CHANNEL;

	/** The PWM engine might be inaccurate on the first clock.
	 * So, send some PWM cycles with duty cycle zero (permanent low).
	 */
	static const unsigned int PREROLL_BITS = 4;

	/** The post-roll is needed at least to set the PWM to 0 % with the last transfer.
	 * The LED strip requires >= 50 us between transfers.
	 * 50 us corresponds to 40 bit (1.7 LEDs) @ 1.25 us */
	static const unsigned int POSTROLL_BITS = 50;

	/**
	 * Length of the entire DMA transfer, including pre-roll and post-roll
	 */
	static const unsigned int PWM_BITS = PREROLL_BITS + PWM_DATA_BITS + POSTROLL_BITS;

	uint16_t pwm_data[PWM_BITS] __attribute__((aligned (32)));;

	Ws2812Impl ws2812_impl;
};

} // namespace WS2812

#endif
