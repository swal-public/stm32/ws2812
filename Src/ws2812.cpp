
#include "../Inc/ws2812.h"

namespace WS2812 {


void Ws2812Impl::setupPwmTimer(TIM_HandleTypeDef *htimer, uint32_t timer_channel, unsigned long clock_hz)
{
	m_htimer = htimer;
	m_timer_channel = timer_channel;

	/* cycle time needs to be 1.25 usec = 1250 ns
	 * Timer's Counter Period (AutoReloadReg):
	 * 72 MHz  ->  89
	 * 	m_timer_pwm_0 = 30;
	 *  m_timer_pwm_1 = 59;
	 *
	 */
	m_timer_autoreload_reg = 1250 * (clock_hz / 1000) / 1000000 - 1;

	/* nsec      H            L          sum
	 * "0"   400 (+-150)  850 (+-150)  1250 (+-300)
	 * "1"   800 (+-150)  450 (+-150)  1250 (+-300)
	 * */

	m_timer_pwm_0 = (400 * (clock_hz / 1000) + 500000) / 1000000 + 1;
	m_timer_pwm_1 = (800 * (clock_hz / 1000) + 500000) / 1000000 + 1;
}


void Ws2812Impl::setupPwmData(uint16_t* pwm_data, unsigned int total_bits, unsigned int preroll_bits, unsigned int data_bits)
{
	m_pwm_data = pwm_data;
	m_total_bits = total_bits;
	m_preroll_bits = preroll_bits;
	m_data_bits = data_bits;

	unsigned int index = 0;

	for (unsigned int i = 0; i < m_preroll_bits; i++) {
		pwm_data[index] = 0;
		index++;
	}

	for (unsigned int i = 0; i < m_data_bits; i++) {
		pwm_data[index] = m_timer_pwm_0;
		index++;
	}

	for (unsigned int i = 0; i < total_bits - m_preroll_bits - m_data_bits; i++) {
		pwm_data[index] = 0;
		index++;
	}

	update();
}


void Ws2812Impl::setPwmData(unsigned int index, uint8_t color)
{
	for (unsigned int i = 0; i < 8; i++) {
		if (color & 0x80) {
			m_pwm_data[index] = m_timer_pwm_1;
		} else {
			m_pwm_data[index] = m_timer_pwm_0;
		}
		index++;
		color <<= 1;
	}
}

void Ws2812Impl::setLedColor(unsigned int led_index, uint8_t r,	uint8_t g, uint8_t b)
{
	unsigned int index = m_preroll_bits + led_index * 3 * 8;

	setPwmData(index, g);
	setPwmData(index + 8, r);
	setPwmData(index + 16, b);
}

void Ws2812Impl::update()
{
	HAL_TIM_PWM_Start_DMA(m_htimer, m_timer_channel, (uint32_t *)m_pwm_data, m_total_bits);
}

} // namespace WS2812
